package org.gradle.rest;

import java.util.ArrayList;
import java.util.List;

import org.gradle.model.Person;
import org.gradle.repo.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/person")
public class PersonController {
	List<Person> persons = new ArrayList<Person>();

	@Autowired
	PersonRepository persRepo;

	@RequestMapping("/")
	public Person getPerson(@RequestParam(value = "id") Long id) {
		return persRepo.findOne(id);
	}

	@RequestMapping("/add")
	@Deprecated public Person addPerson(@RequestParam(value = "name") String name, //
			@RequestParam(value = "age") Integer age, //
			@RequestParam(value = "location") String location) {
		Person entity = new Person(name, age, location);
		return persRepo.save(entity);
	}
	
	@RequestMapping(value="/add", method = RequestMethod.POST)
	public Person addPerson(@RequestBody Person person){
		return persRepo.save(person);
	}
	
	@RequestMapping("/all")
	public List<Person> getPersons() {
		Iterable<Person> findAll = persRepo.findAll();
		List<Person> pers = new ArrayList<>();
		for (Person p : findAll) {
			pers.add(p);
		}
		return pers;
	}
}
