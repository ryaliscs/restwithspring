package org.gradle.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.*;
import org.gradle.model.StockXlsx;

public class ReadXlsx {

	public static void main(String args[]) {
		ReadXlsx read = new ReadXlsx();
		List<StockXlsx> readXlsx = read.readXlsx("c:\\expws\\stock.xlsx");
		System.out.println(readXlsx.toString());
	}

	public List<StockXlsx> readXlsx(String aFilePath) {
		List<StockXlsx> stocks = new ArrayList<StockXlsx>();
		try {
			FileInputStream excelFile = new FileInputStream(new File(aFilePath));
			Workbook workbook = new XSSFWorkbook(excelFile);
			Sheet datatypeSheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = datatypeSheet.iterator();
			boolean isHeader = true;
			int columnCount = 0;
			while (iterator.hasNext()) {
				Row currentRow = iterator.next();
				Iterator<Cell> cellIterator = currentRow.iterator();
				StockXlsx stock = new StockXlsx();
				// skip header
				if (!isHeader) {
					while (cellIterator.hasNext()) {
						columnCount = columnCount + 1;
						Cell currentCell = cellIterator.next();
						 setStockValue(stock, currentCell, columnCount);
						/*System.out.println("Count :"+count);
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							System.out.print(currentCell.getStringCellValue() + "-- ");
						} else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
							System.out.print(currentCell.getNumericCellValue() + "--");
						}*/

					}
					columnCount=0;
					stocks.add(stock);
					 System.out.println();
				} else {
					isHeader = false;
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return stocks;
	}

	private void setStockValue(StockXlsx stock, Cell currentCell, int count) {
		switch (count) {
		case 1:
			stock.setCode(String.valueOf(currentCell.getNumericCellValue()));
			break;
		case 2:
			stock.setName(currentCell.getStringCellValue());
			break;
		case 3:
			stock.setQuantity(currentCell.getNumericCellValue());
			break;
		}
	}
}
