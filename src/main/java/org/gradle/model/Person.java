package org.gradle.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Person {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
    private String name;
    private Integer age;
    private String location;
    
    protected Person() {
    	
    }

    public Person(String name, Integer age, String location) {
    	this.name = name;
        this.age = age;
        this.location = location;
    }

    public String getName() {
        return name;
    }
    
    public Long getId() {
        return id;
    }

	public Integer getAge() {
		return age;
	}

	public String getLocation() {
		return location;
	}
	
	@Override
	public String toString() {
	
		return this.id + " - " + this.name;
	}
}
