package org.gradle.model;

import java.io.Serializable;

public class StockXlsx implements Serializable {

	private String code;
	private String name;
	private double quantity;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}
	
	public String toString()
	{
		return String.join(",", this.code, this.name, Double.toString(this.quantity));
	}
}
