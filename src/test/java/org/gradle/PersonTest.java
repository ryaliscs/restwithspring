package org.gradle;

import org.junit.Test;
import static org.junit.Assert.*;

import org.gradle.model.Person;

public class PersonTest {
    @Test
    public void canConstructAPersonWithAName() {
        Person person = new Person( "Larry", Integer.valueOf(10), "New york");
        assertEquals("Larry", person.getName());
    }
}
