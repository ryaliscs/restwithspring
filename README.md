# README #

This is a quick start to create Rest web services using spring boot along with spring jpa

configuration to connect to H2 (in-memory) or MySql

### What is this repository for? ###

* Quick summary
Rest implementation with Spring boot and Sprint JPA
* Version
1.0

### How do I get set up? ###

* Summary of set up  
   Check out the project  
   import the project into eclipse as Gradle(STS) project  
   build/ctrl+f5  
  
* Configuration  
  refer: build.gradle  

* Dependencies  
  refer: build.gradle  

* Database configuration  
	H2 data base
* How to run tests  
* Deployment instructions  
* Running the application  
	run as java application and chose org.gradle.Aplication
	
### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### References ###  

https://spring.io/guides/gs/accessing-data-jpa/


### Who do I talk to? ###

* Repo owner or admin
Sarat Chandra Sekhar R  
email: ryaliscs@gmail.com